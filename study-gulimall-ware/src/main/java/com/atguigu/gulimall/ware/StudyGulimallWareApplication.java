package com.atguigu.gulimall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyGulimallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudyGulimallWareApplication.class, args);
    }

}
