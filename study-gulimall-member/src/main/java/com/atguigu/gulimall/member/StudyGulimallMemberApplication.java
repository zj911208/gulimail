package com.atguigu.gulimall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyGulimallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudyGulimallMemberApplication.class, args);
    }

}
