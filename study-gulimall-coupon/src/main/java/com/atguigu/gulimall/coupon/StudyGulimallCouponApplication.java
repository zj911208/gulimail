package com.atguigu.gulimall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyGulimallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudyGulimallCouponApplication.class, args);
    }

}
